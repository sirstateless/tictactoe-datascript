(ns steps.setup)

(defn ^:export alwaysTrue [] true)

(defn ^:export setupTestShouldPass [] (assert true))

(defn ^:export squaring [a b]
    (assert (= (* a a) b)))
