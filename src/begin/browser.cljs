(ns begin.browser
    (:require
        ["datascript" :as d]))

;;(defn ^:export ds [] 
;;    (clj->js d))

(defn ^:dev/before-load stop []
    (js/console.log "shutting down"))

(defn ^:dev/after-load start []
    (js/console.log "done loading changes"))

(defn ^:export init [] 
    (js/console.log "init")
    (start))
