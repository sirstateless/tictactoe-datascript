# tictactoe-datascript

[![license](https://img.shields.io/badge/license-MIT-brightgreen.svg)](LICENSE)

This project is a proof of concept, and personal testing area for learning more about the use of [shadow-cljs], [datascript], [reframe] and [cucumber].

This is not meant for serious use, but may be a helpful reference for software developers when learning these technologies.

## How to use this project

1.  Clone this repository
2.  Install the dependencies using `yarn` (or `npm install`)
3.  Run the project locally using `yarn start` (or `npm start`)
4.  Run the tests locally using `yarn test` (or `npm test`)

[cucumber]: https://github.com/cucumber/cucumber-js
[datascript]: https://github.com/tonsky/datascript
[reframe]: https://github.com/Day8/re-frame
[shadow-cljs]: http://shadow-cljs.org/
