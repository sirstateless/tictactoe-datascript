Feature: setup
    In Order To verify test setup
    As a developer
    I want to see this scenario pass

    Scenario: proof of setup
        Given a very simple Feature
        When step definitions are found
        Then setup test should pass

    Scenario: parameters setup
        Given a very simple Feature
        When step definitions are found
        Then squaring 2 should be 4
