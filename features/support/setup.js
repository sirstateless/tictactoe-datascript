const { Given, When, Then } = require("cucumber");

const steps = require(".ttt/steps.setup");

Given(`a very simple Feature`, steps.alwaysTrue);
When(`step definitions are found`, steps.alwaysTrue);
Then(`setup test should pass`, steps.setupTestShouldPass);
Then(`squaring {int} should be {int}`, steps.squaring);
